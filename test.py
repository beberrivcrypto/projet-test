import base64
with open("test.jpg", "rb") as img_file:
    my_string = base64.b64encode(img_file.read())
f = open("result.txt", "a")
f.write(my_string.decode("utf-8"))
f.close
